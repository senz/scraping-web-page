const Nightmare = require('nightmare')
const nightmare = Nightmare()
const process = require('process');

nightmare
	.goto('https://codequiz.azurewebsites.net/')
	.click("input[type='button']")
	.evaluate(
		() => {
            var wordList = []
			document.querySelectorAll('tr').forEach(function(result , index){
                if (index === 0) return true;

                const tds = result.querySelectorAll("td");
                    let word = {
                        'fundNmae' : tds[0].textContent,
                        'nav' : tds[1].textContent,
                        'bid' : tds[2].textContent,
                        'offer' : tds[3].textContent,
                        'change' : tds[4].textContent,
                    }
                    return wordList.push(word)
            })
            return wordList
        }
    )
	.end()
	.then((link) => {
        link.forEach((rs , i)=>{
            if (process.argv.slice(2)[0] == rs.fundNmae.trim()){
                console.log(rs.nav)
            }
        })
		// console.log('Scraping Bee Web Link:', link)
	})
	.catch((error) => {
		console.error('Search failed:', error)
	})